FROM ubuntu:20.04
LABEL maintainer="info@axonibyte.com"
RUN apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install curl jq bash -y \
    && addgroup chainsvc \
    && useradd -g chainsvc -d /chain chainsvc
COPY build/iris /bin/iris
RUN chmod +x /bin/iris
STOPSIGNAL SIGTERM
USER chainsvc
WORKDIR /chain
ENTRYPOINT ["/bin/iris", "start", "--home", "/chain"]
